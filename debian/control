Source: openpilot
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               gcc-arm-none-eabi,
               python-all-dev,
               libqt5opengl5-dev,
               qtbase5-dev,
               qtbase5-dev-tools,
               qttools5-dev-tools,
               libnewlib-arm-none-eabi,
               libstdc++-arm-none-eabi-newlib,
               qtdeclarative5-dev,
               libqt5svg5-dev,
               qtscript5-dev,
               libusb-1.0-0-dev,
               libqt5serialport5-dev,
               qtmultimedia5-dev,
               libsdl1.2-dev,
               libudev-dev,
               binutils-arm-none-eabi (= 2.25-3+7),
               qml-module-qtquick-controls,
               libeigen3-dev,
               zlib1g-dev,
               lib3ds-dev,
               libgtkglext1-dev,
               libquazip-qt5-dev,
               libquazip-headers
Standards-Version: 3.9.6
Vcs-Browser: https://anonscm.debian.org/cgit/debian-science/packages/openpilot.git
Vcs-Git: git://anonscm.debian.org/debian-science/packages/openpilot.git
Homepage: http://www.openpilot.org

Package: openpilot
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqt5multimedia5-plugins,
         qml-module-qtquick-controls,
         qml-module-qtquick-xmllistmodel,
         openpilot-data (= ${source:Version})
Description: Platform for aerial robotics or other mobile vehicular platforms
 OpenPilot is a next-generation Open Source UAV autopilot created by the 
 OpenPilot Community (an all volunteer non-profit community). It is a highly 
 capable platform for multirotors, helicopters, fixed wing aircraft, and
 other vehicles. 
 .
 It has been designed from the ground up by a community of passionate 
 developers from around the globe, with its core design principles being
 quality, safety, and ease of use.  Simplicity does not come with any
 compromises either: with no hard-coded settings, a complete flight plan
 scripting language and other powerful features, OpenPilot is an extremely
 capable UAV autopilot platform.
 .
 Package contains binaries and libraries

Package: openpilot-data
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Platform for aerial robotics. Data files
 OpenPilot is a next-generation Open Source UAV autopilot created by the 
 OpenPilot Community (an all volunteer non-profit community). It is a highly 
 capable platform for multirotors, helicopters, fixed wing aircraft, and
 other vehicles. 
 .
 It has been designed from the ground up by a community of passionate 
 developers from around the globe, with its core design principles being
 quality, safety, and ease of use.  Simplicity does not come with any
 compromises either: with no hard-coded settings, a complete flight plan
 scripting language and other powerful features, OpenPilot is an extremely
 capable UAV autopilot platform.
 .
 Package contains data files
